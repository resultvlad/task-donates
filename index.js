import './index.css';
import FormatDate from './utils/format-date';
import { DonateItem } from './utils/donate-item';
import LocalStorageUpdate from './utils/local-storage-update';

LocalStorageUpdate()

const donateForm = document.querySelector('.donate-form')
donateForm.addEventListener('click', e => {
    e.preventDefault()

    const donateFormDonateInput = document.querySelector('.donate-form__donate-input')

    if ((e.target.className === 'donate-form__submit-button')) { 
        const newDonate = new DonateItem(FormatDate(), Number(donateFormDonateInput.value).toFixed(2))
        LocalStorageUpdate(newDonate)
        donateFormDonateInput.value = ''
    }
})