export class DonateItem {
    constructor(date, sum) {
        this.date = date,
        this.sum = sum
    }

    render() {
        const donatesContainerDonates = document.querySelector('.donates-container__donates')
        const donateItem = document.createElement('div')
        donateItem.className = 'donate-item'
        donateItem.textContent = `${this.date} - `
        donatesContainerDonates.append(donateItem)

        const donateSum = document.createElement('b')
        donateSum.textContent = `${this.sum}$`
        donateItem.append(donateSum)

        if (document.querySelector('.init-donate-item')) {
            document.querySelector('.init-donate-item').remove()
        }
    }
}