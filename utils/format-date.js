import moment from "moment"
import 'moment-precise-range-plugin'

export default function FormatDate() {
    const date = new Date()
    return moment(date).format('MMMM Do YYYY, h:mm:ss a')
}