export default function InitData() {
    const totalAmount = document.querySelector('#total-amount')
    const donatesContainerDonates = document.querySelector('.donates-container__donates')

    totalAmount.textContent = `0$`

    const donateItem = document.createElement('div')
    donateItem.classList.add('donate-item', 'init-donate-item')
    donateItem.textContent = `Ещё никто не задонатил. Станьте первым!`
    donatesContainerDonates.append(donateItem)
}