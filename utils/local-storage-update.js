import { DonateItem } from "./donate-item"
import { TOTAL_DONATES, EVERY_DONATES } from "./local-storage-keys"
import InitData from "./init-data"

export default function LocalStorageUpdate(newDonate) {

    const totalAmount = document.querySelector('#total-amount')
    let newDonates = []

    if (!newDonate && !localStorage.getItem(TOTAL_DONATES)) {
        localStorage.setItem(TOTAL_DONATES, '0')
        localStorage.setItem(EVERY_DONATES, '[]')

        InitData()
    } else if (!newDonate && localStorage.getItem(TOTAL_DONATES)) {
        if (Number(localStorage.getItem(TOTAL_DONATES)) < 1) {
            InitData()
        } else {
            totalAmount.textContent = `${localStorage.getItem(TOTAL_DONATES)}$`
            let donates = JSON.parse(localStorage.getItem(EVERY_DONATES))
            donates.forEach(e => {
                let donate = new DonateItem(e.date, e.sum)
                donate.render()
            })
        }
    } else if (newDonate.sum >= 1 && newDonate.sum <= 100) {
        let newAmount = Number(localStorage.getItem(TOTAL_DONATES)) + Number(newDonate.sum)
        localStorage.setItem(TOTAL_DONATES, newAmount.toFixed(2))
        totalAmount.textContent = `${localStorage.getItem(TOTAL_DONATES)}$`

        newDonates = JSON.parse(localStorage.getItem(EVERY_DONATES))
        newDonates.push(newDonate)
        localStorage.setItem(EVERY_DONATES, JSON.stringify(newDonates))

        newDonate.render()
    } else {
        alert('Сумма доната не может быть меньше 1$ и больше 100$')
    }
}